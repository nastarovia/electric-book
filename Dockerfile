include(bionic)

RUN apt update && apt upgrade --yes
RUN apt install --yes git wget curl unzip

### install ruby and its dependencies for native gems
RUN apt install --yes ruby ruby-dev make gcc build-essential

### install jekyll and bundler
RUN gem update --system --no-ri --no-rdoc &&\
    gem install jekyll &&\
    gem install bundler

### install PrinceXML
RUN wget https://www.princexml.com/download/prince_11.4-1_ubuntu18.04_amd64.deb &&\
    apt install --yes ./prince_11.4-1_ubuntu18.04_amd64.deb

### install PhantomJS
RUN apt install --yes chrpath libssl-dev libxft-dev \
                      libfreetype6 libfreetype6-dev \
		      libfontconfig1 libfontconfig1-dev &&\
    wget https://bitbucket.org/ariya/phantomjs/downloads/phantomjs-2.1.1-linux-x86_64.tar.bz2 &&\
    tar xvjf phantomjs-2.1.1-linux-x86_64.tar.bz2 -C /usr/local/share/ &&\
    ln -sf /usr/local/share/phantomjs-2.1.1-linux-x86_64/bin/phantomjs /usr/local/bin/

### install Pandoc
RUN wget https://github.com/jgm/pandoc/releases/download/2.5/pandoc-2.5-1-amd64.deb &&\
    apt install --yes ./pandoc-2.5-1-amd64.deb

### install Node and Gulp
RUN apt install --yes nodejs npm &&\
    npm install --global gulp-cli

### install GraphicsMagick
RUN apt install --yes graphicsmagick

### install apache2
RUN apt install --yes apache2 && a2enmod ssl
