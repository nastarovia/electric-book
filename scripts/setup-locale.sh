#!/bin/bash -x

#source /host/settings.sh

### setup locale
sed -i /etc/locale.gen \
    -e '/en_US.UTF-8/ c en_US.UTF-8 UTF-8'
locale-gen
update-locale --reset \
              LANG="en_US.UTF-8" \
              LANGUAGE="en_US:en" \
              LC_ALL="en_US.UTF-8"
sed -i /etc/bash.bashrc -e '/export LC_ALL=/d'
cat <<EOF >> /etc/bash.bashrc
export LC_ALL=en_US.UTF-8
EOF
