cmd_config_help() {
    cat <<_EOF
    config
        Run configuration scripts inside the container.

_EOF
}

cmd_config() {
    ds inject ubuntu-fixes.sh
    ds inject setup-locale.sh

    # create a sample book
    if [[ ! -d books ]]; then
        mkdir books
        local dir=books/book1.example.org
        ds ebook init $dir
        ds ebook install $dir
        ds ebook render $dir
        ds site add book1.example.org $dir
    fi
}
